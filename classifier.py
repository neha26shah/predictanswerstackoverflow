import pickle
import operator
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
import sys
import xmltodict
fmodel = open("tfidfvectorizer.pkl")
count_vect = pickle.load(fmodel)
fmodel.close()
question_best={}
question_vectors = {}
question_scores={}
with open('../data/AcceptedQuestions.xml') as fd:
    obj = xmltodict.parse(fd.read())
for item in  obj['posts']['row']:
	body_value=""
	rowid= ""
	try:
		rowid=  item["@Id"]
		body_value = item["@Body"]
	except:
		continue
	vec = count_vect.transform([body_value])
	question_vectors[rowid] = vec
	best_answer = item["@AcceptedAnswerId"]
	question_best[rowid] = best_answer
	question_scores[rowid] = {}	
with open('../data/AcceptedAnswers.xml') as fd:
    obj = xmltodict.parse(fd.read())
for item in  obj['posts']['row']:
	body_value=""
	rowid= ""
	parentid=""
	try:
		rowid=  item["@Id"]
		body_value = item["@Body"]
		parentid = item["@ParentId"]
	except:
		continue
	answer_vec=  count_vect.transform([body_value])
	question_vec = question_vectors[parentid]
	a = answer_vec.toarray().flatten()
	q = question_vec.toarray().flatten()
	dot_product = np.dot(a,q)
	question_scores[parentid][rowid] = dot_product
print question_scores
mrr=0
for question in question_scores.keys():
	best_answer = question_best[question]
	sorted_x = sorted(question_scores[question].items(),key = operator.itemgetter(1),reverse=True)
	rank=0
	for i in range(0,len(sorted_x)):
		if sorted_x[i][0] == best_answer:
			rank=i+1
			break
	if rank==0:
		continue
	mrr = mrr+(1.0/rank)
print mrr
mrr = mrr/len(question_scores)
print mrr	
