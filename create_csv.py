#!usr/bin/python
all_users = []
all_tags = []
numTopics = 30
def read_users(filename):
	fp = open(filename,"r")
	while True:
		line = fp.readline()
		if not line:
			break
		tokens = line.split()
		all_users.append((tokens[0]))
def read_tags(filename):
	fp = open(filename,"r")
	while True:
		line = fp.readline()
		if not line:
			break
		tokens = line.split()
		all_tags.append(tokens[0])
def combine(probf,tagf,userf):
	p = open(probf,"r")
	t = open(tagf,"r")
	u = open(userf,"r")
	while True:
		linep = p.readline()
		linet = t.readline()
		lineu = u.readline()
		if not lineu:
			break
		prob = linep.split()
		tag = linet.split()
		users = lineu.split()	
		if not(int(prob[0]) == int(tag[0]) and int(tag[0]) == int(users[0])):
			print "NOt compatible"
			break
		string = ""	
		prob.pop(0)		
		tag.pop(0)
		users.pop(0)
		for k in prob:
			string = string + str(k) + ","
		for tg in all_tags:
			if tg in tag:
				string = string  + "1,"
			else:
				string = string  + "0,"
		for us in all_users:
			if us in users:
				string = string  + "1,"
			else:
				string = string  + "0,"
		print string[:-1]
read_users("all_users.txt")
read_tags("shortlisted_tags.txt")
string = ""
for i in range(1,numTopics+1):
	string = string + "topic" + str(i) + ","
for tg in all_tags:
	string = string + tg + ","
for us in all_users:
	string = string + us + ","
print string[:-1]
#print len(all_users) + len(all_tags)
combine("probability_file.txt","tags.txt","questionusers.txt")
