from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
import xmltodict
import pickle
content=[]
count_vect = TfidfVectorizer(decode_error="ignore",stop_words = 'english',lowercase=True)
with open('../data/AcceptedQuestions.xml') as fd:
    obj = xmltodict.parse(fd.read())
for item in  obj['posts']['row']:
	body_value=""
	try:
		body_value = item["@Body"]
	except:
		continue
	content.append(body_value)
with open('../data/AcceptedAnswers.xml') as fd:
    obj = xmltodict.parse(fd.read())
for item in  obj['posts']['row']:
	body_value=""
	try:
		body_value = item["@Body"]
	except:
		continue
	content.append(body_value)
count_vect.fit(content)
fout = open("tfidfvectorizer.pkl","w")
pickle.dump(count_vect,fout)
fout.close()
print len(count_vect.vocabulary_)
