#!usr/bin/python
all_qs= []
probability = []
numTopics = 30
# this function will read all the probability values from the given file and arrange them topic wise.
# The corresponding question ids are stored in all_qs
def read_probability(filename):
	p = open(filename,"r")
	dump = p.readline()
	while True :
		line = p.readline()
		if not line:
			break
		tokens = line.split()
		all_qs.append(int(tokens[1]))
		temp = []
		for i in range(0,numTopics):
			temp.append(0.0)
		k= 2
		while k <= numTopics*2 :
			temp[int(tokens[k])] = float(tokens[k+1])
			k= k+2
		probability.append(temp)
#	for k in probability:
#		print k
#Out of the probability distribution for all the questions. Filter out those questions which are in the training set.
# The questions in the training set are given by the input filename
def only_training(filename):
	q = open(filename,"r")
	while True:
		line = q.readline()
		if not line :
			break
		tokens = line.split()
		qid = int(tokens[0])
		index = all_qs.index(qid)
		string  = ""
		for k in probability[index]:
			string = string + str(k) + " "
#		print str(qid)+ " " + str(probability[all_qs.index(qid)])
		print str(qid)+ " " + string
read_probability("Aprobability30_20.txt")
only_training("trainingQuestions.txt")
